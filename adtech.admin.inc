<?php

/**
 * @file
 * Contains the administrative functions of the ADTECH module.
 *
 * This file is included by the ADTECH module, and includes the
 * settings form.
 *
 * @author Gideon Cresswell <gideon@gideoncresswell.com>
 * http://www.gideoncresswell.com
 *
 * Drupal ID: SkidNCrashwell
 */

/**
 * Page callback function to display all available adverts
 */
function adtech_list_adverts() {
  $header = array(
    t('Alias'),
    t('Size'),
    t('Block'),
    t('Placement ID'),
    t('Created'),
    t('Active'),
    t('Operations'),
  );
  $rows = array();
  $ad_formats = adtech_ad_formats();
  $blocks = adtech_get_blocks();

  foreach (adtech_get_ads() as $advert) {
    $format = $ad_formats[$advert['size_id']];
    $rows[] = array(
      'alias'     => t($advert['alias']),
      'size'      => $format['name'] . ' ' . $format['size'],
      'block'     => $blocks[$advert['block_id']]['title'],
      'placement' => $advert['placement_id'],
      'created'   => date('j M y, H:i:s', $advert['created']),
      'active'    => ($advert['status'] == 1) ? '<strong>Active</strong>' : 'Inactive',
      'operations'   => l(t('Edit'), 'admin/settings/adtech/adverts/' . $advert['advert_id'] . '/edit') . '&nbsp;&nbsp;&nbsp;&nbsp;' . l(t('Delete'), 'admin/settings/adtech/adverts/' . $advert['advert_id'] . '/delete'),
    );
  }
  
  if (empty($rows)) {
    $output = t('<p>Add some blocks first from the <a href="@block">add new block</a> page, then <a href="@adverts">add adverts</a>.</p>', array('@block' => url('admin/settings/adtech/blocks/add'), '@adverts' => url('admin/settings/adtech/adverts/add')));
  }
  else {
    $output = t('<p>Make sure you have set up <a href="@blocks">blocks</a> first to place your adverts in.</p>', array('@blocks' => url('admin/settings/adtech/blocks')));
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, 30);
  }

  return $output;
}

/**
 * Page callback function to display either add or edit advert form
 */
function adtech_admin_advert($advert_id = NULL) {
  if ($advert_id) {
    $advert = adtech_advert_load($advert_id);
    $output = drupal_get_form('adtech_admin_advert_form', $advert);
  }
  else {
    $output = drupal_get_form('adtech_admin_advert_form');
  }
  return $output;
}

/**
 * Add or edit advert form
 * If an advert array is passed to the function then it becomes an edit form
 */
function adtech_admin_advert_form(&$form_state, $advert = array('advert_id' => NULL, 'visibility' => 0, 'taxonomy' => 0, 'status' => 1)) {
  $form = array();
  $ad_formats = adtech_ad_formats();
  $blocks = adtech_get_blocks();
  $visibility_options = adtech_get_visibility_options();

  foreach ($ad_formats as $creative) {
    $creative_title = t($creative['size_id'] . ' - ' . $creative['name'] . ' ' . $creative['size']);
    $creative_options[$creative['size_id']] = $creative_title;
  }

  foreach ($blocks as $block) {
    $block_options[$block['block_id']] = t($block['title']);
  }
  
  if (variable_get('adtech_auto_alias', FALSE) == TRUE) {
    $form['alias'] = array(
      '#type'          => 'textfield',
      '#disabled'      => TRUE,
      '#title'         => t('Advert placement alias'),
      '#value'         => 'Auto generated',
      '#default_value' => $advert['alias'],
      '#maxlength'     => 128,
      '#description'   => t('Placement aliases are automatically being generated. Please ensure that the ADTECH system is aware of the alias.'),
    );
  }
  else {
    $form['alias'] = array(
      '#type'        => 'textfield',
      '#title'       => t('Advert placement alias'),
      '#default_value' => $advert['alias'],
      '#required'      => TRUE,
      '#maxlength'   => 128,
      '#description' => t('Enter the placement alias here from the ADTECH system. You can also use tokens to set up patterns.'),
    );
    $form['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['token_help']['help'] = array(
      '#value' => theme('token_help', 'global'),
    );
  }
  $form['placement_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Advert placement id'),
    '#maxlength'     => 128,
    '#size'          => 20,
    '#default_value' => $advert['placement_id'],
    '#required'      => TRUE,
    '#description'   => t('Enter the placement id here from the ADTECH system.'),
  );
  $form['size_id'] = array(
    '#type'          => 'select',
    '#title'         => t('Creative size'),
    '#options'       => $creative_options,
    '#default_value' => $advert['size_id'],
    '#required'      => TRUE,
    '#description'   => t('Select the creative size for this advert.'),
  );
  $form['block_id'] = array(
    '#type'          => 'select',
    '#title'         => t('Block to show advert in'),
    '#options'       => $block_options,
    '#default_value' => $advert['block_id'],
    '#required'      => TRUE,
    '#description'   => t('Select the block to show this advert in. Ensure you choose a block that will fit this creative or else it may get cropped.'),
  );
  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific visibility settings'),
    '#collapsible' => TRUE,
  );
  $form['page_vis_settings']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show advert on specific pages'),
    '#options' => $visibility_options,
    '#default_value' => $advert['visibility'],
  );
  $form['page_vis_settings']['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => $advert['pages'],
    '#description' => t("<p>Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.</p><p>Note that options dictate the priority, so 'Show on every page except the listed pages' will show if an advert is supposed to show on this page over and one that is set to only appear on the listed pages.</p>", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );
  if (variable_get('adtech_group_generator', TRUE) === 0) {
    $form['group_id'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Group ID'),
      '#maxlength'     => 9,
      '#size'          => 20,
      '#default_value' => $advert['group_id'],
      '#description'   => t('Add an optional group ID for this advert which needs to be the same for all placements on the same page.'),
    );
  }
  $form['keywords'] = array(
    '#type' => 'fieldset',
    '#title' => t('Keywords'),
    '#collapsible' => TRUE,
    '#description' => t('Up to ten keywords can be passed within one ad request. This is only necessary if you use keywords.'),
  );
  $form['keywords']['taxonomy'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use taxonomy terms'),
    '#default_value' => $advert['taxonomy'],
    '#description'   => t('Select to automatically include any taxonomy terms that are on the page as keywords. <span class="warning">Taxonomy terms will take precedence and will override any specific terms below if there are more than ten keywords for any page.</span>'),
  );
  $form['keywords']['keyword_1'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_1'],
  );
  $form['keywords']['keyword_2'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_2'],
  );
  $form['keywords']['keyword_3'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_3'],
  );
  $form['keywords']['keyword_4'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_4'],
  );
  $form['keywords']['keyword_5'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_5'],
  );
  $form['keywords']['keyword_6'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_6'],
  );
  $form['keywords']['keyword_7'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_7'],
  );
  $form['keywords']['keyword_8'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_8'],
  );
  $form['keywords']['keyword_9'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_9'],
  );
  $form['keywords']['keyword_10'] = array(
    '#type' => 'textfield',
    '#maxlength'     => 60,
    '#default_value' => $advert['keyword_10'],
  );
  $form['status'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Active'),
    '#default_value' => $advert['status'],
    '#description'   => t('Select to make this advert appear on the site.'),
  );
  if ($advert['advert_id']) {
    $form['advert_id'] = array(
      '#type' => 'hidden',
      '#value' => $advert['advert_id'],
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update advert'),
      '#weight' => 9,
    );
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add new advert'),
      '#weight' => 9,
    );
  }
  
  return $form;
}

/**
 * Add/Edit form validator function
 *
 * Checks placement ID is numeric
 * Checks if the advert is to be shown or excluded on certain pages that the path value is provided
 */
function adtech_admin_advert_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['placement_id'])) {
    form_set_error('placement_id', t('Placement ID must be a valid number.'));
  }
  if ($form_state['values']['visibility'] < 2 && empty($form_state['values']['pages'])) {
    form_set_error('pages', t('You must provide paths to @str this advert on.', array('@str' => ($form_state['values']['visibility'] == 1) ? 'include' : 'exclude')));
  }
}

/**
 * Form submit function
 * Saves the advert into the database
 */
function adtech_admin_advert_form_submit($form, &$form_state) {
  $advert = new stdClass();

  // Alias
  if (variable_get('adtech_auto_alias', FALSE) == TRUE) {
    $advert->alias = 'Automatic';
  }
  else {
    $advert->alias = $form_state['values']['alias'];
  }
  
  $advert->placement_id = $form_state['values']['placement_id'];
  $advert->size_id      = $form_state['values']['size_id'];
  $advert->block_id     = $form_state['values']['block_id'];
  $advert->visibility   = $form_state['values']['visibility'];
  $advert->pages        = $form_state['values']['pages'];
  $advert->taxonomy     = $form_state['values']['taxonomy'];
  
  // Format keywords. If blanks are left in form then any keywords below shift up
  $remove_chars = array("\r\n", "\n", "\r", '+', '"', "'");
  $j = 1;
  for ($i = 1; $i < 11; $i++) {
    if (!empty($form_state['values']['keyword_' . $i])) {
      $keyword = 'keyword_' . $j;
      $advert->$keyword = str_replace($remove_chars, '', $form_state['values']['keyword_' . $i]);
      $j++;
    }
  }
  
  $advert->status = $form_state['values']['status'];

  if ($form_state['values']['advert_id']) {
    $advert->advert_id = $form_state['values']['advert_id'];
    if (drupal_write_record('adtech_adverts', $advert, 'advert_id')) {
      drupal_set_message($advert->alias . ' advert updated.');
      $form_state['redirect'] = 'admin/settings/adtech';
    }
    else {
      drupal_set_message('There was an error updating ' . $advert->alias . ' advert. Please try again.');
    }
  }
  else {
    $advert->created = time();
    if (drupal_write_record('adtech_adverts', $advert)) {
      drupal_set_message($advert->alias . ' advert added.');
    }
    else {
      drupal_set_message('There was an error adding ' . $advert->alias . ' advert. Please try again.');
    }
  }
}
/**
 * Confirm deletion of an advert function
 */
function adtech_admin_delete_confirm_advert($form_state, $advert_id) {
  $advert = adtech_advert_load($advert_id);
  $form['advert_id'] = array(
    '#type' => 'value',
    '#value' => $advert_id,
  );

  $output = confirm_form($form, t('Are you sure you want to delete advert %alias?', array('%alias' => $advert->alias)), 'admin/settings/adtech');

  return $output;
}

/**
 * Page callback function to display either add or edit advert form
 */
function adtech_admin_delete_confirm_advert_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    adtech_admin_delete_advert($form_state['values']['advert_id']);
    $form_state['redirect'] = 'admin/settings/adtech';
    return;
  }
}

/**
 * Page callback function to display either add or edit advert form
 */
function adtech_admin_delete_advert($advert_id = 0) {
  db_query('DELETE FROM {adtech_adverts} WHERE advert_id = %d LIMIT %d', $advert_id, 1);
  db_query('OPTIMIZE TABLE {adtech_adverts}');
  drupal_set_message(t('The advert has been deleted.'));
}

/**
 * Lists all ADTECH blocks set up in a table with links to edit/delete
 */
function adtech_list_blocks() {
  $header = array(
    t('Block id'),
    t('Block name'),
    t('Caching'),
    t('Active'),
    t('Operations'),
  );
  $rows = array();
  $blocks = adtech_get_blocks();
  $caching = adtech_caching_options();
  foreach ($blocks as $block) {
    $rows[] = array(
      'id'      => $block['block_id'],
      'name'    => t($block['title']),
      'cache'   => t($caching[$block['caching']]),
      'active'  => ($block['status'] == 1) ? '<strong>Active</strong>' : 'Inactive',
      'operations' => l(t('Edit'), 'admin/settings/adtech/blocks/' . $block['block_id'] . '/edit') . '&nbsp;&nbsp;&nbsp;&nbsp;' . l(t('Delete'), 'admin/settings/adtech/blocks/' . $block['block_id'] . '/delete'),
    );
  }
  if (empty($rows)) {
    $output = t('<p>Add some blocks first from the <a href="@block">add new block</a> page.</p>', array('@block' => url('admin/settings/adtech/blocks/add')));
    }
  else {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, 30);
  }
  return $output;
}

/**
 * Callback function for the add/edit block link
 */
function adtech_admin_block($block_id = NULL) {
  if ($block_id) {
    $block = adtech_block_load($block_id);
    $output = drupal_get_form('adtech_admin_block_form', $block);
  }
  else {
    $output = drupal_get_form('adtech_admin_block_form');
  }

  return $output;
}

/**
 * Form used on the add/edit block. If a $block array is passed with a valid block ID then that block will be edited
 */
function adtech_admin_block_form(&$form_state, $block = array('block_id' => NULL, 'title' => '', 'caching' => BLOCK_CACHE_PER_PAGE, 'status' => 1)) {
  $form = array();
  $title_options = array();
  $cache_options = adtech_caching_options();
  $ad_formats = adtech_ad_formats();

  foreach ($ad_formats as $ad) {
    $title = t($ad['name'] . ' ' . $ad['size']);
    $title_options[$title] = $title;
  }
  
  if ($block['block_id']) {
    $form['title'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Block title'),
      '#default_value' => $block['title'],
      '#description'   => t('Edit the name of this block.'),
    );
  }
  else {
    $form['title'] = array(
      '#type'          => 'select',
      '#title'         => t('Block title'),
      '#options'       => $title_options,
      '#description'   => t('Select the type of advert this block will be used to display.'),
    );
  }
  $form['caching'] = array(
    '#type'          => 'select',
    '#title'         => t('Block caching'),
    '#default_value' => $block['caching'],
    '#options'       => $cache_options,
    '#description'   => t('Set the caching this block should use. If unsure, leave as the default <strong>BLOCK_CACHE_PER_PAGE</strong> so the block is only cached per page. If a block is to show only one placement ID sitewide, then use <strong>BLOCK_CACHE_GLOBAL</strong>.'),
  );
  $form['active'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Active'),
    '#default_value' => $block['status'],
    '#description'   => t('Select to make this block available on the <a href="@block">blocks admin page</a>.', array('@block' => url('admin/build/block'))),
  );
  if ($block['block_id']) {
    $form['block_id'] = array(
      '#type' => 'hidden',
      '#value' => $block['block_id'],
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update block'),
      '#weight' => 9,
    );
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add new block'),
      '#weight' => 9,
    );
  }

  return $form;
}

/**
 * Validate block form.
 *
 * Checks that a block name is set otherwise it will not submit
 */
function adtech_admin_block_form_validate($form, &$form_state) {
  if (empty($form_state['values']['title'])) {
    form_set_error('title', t('You must specify a block name!'));
  }
}

/**
 * Writes the block to the database
 */
function adtech_admin_block_form_submit($form, &$form_state) {
  $block = new stdClass();
  $block->title   = $form_state['values']['title'];
  $block->caching = $form_state['values']['caching'];
  $block->status  = $form_state['values']['active'];

  if ($form_state['values']['block_id']) {
    $block->block_id = $form_state['values']['block_id'];
    if (drupal_write_record('adtech_blocks', $block, 'block_id')) {
      drupal_set_message($block->title . ' block updated.');
      $form_state['redirect'] = 'admin/settings/adtech/blocks';
    }
    else {
      drupal_set_message('There was an error updating ' . $block->title . ' block. Please try again.');
    }
  }
  else {
    // Check if the title already exists in the DB first
    $count = db_result(db_query('SELECT COUNT(block_id) FROM {adtech_blocks} WHERE title LIKE "%s%%"', $block->title));
  
    // If so, append the title an additional number identifier
    if ($count > 0) {
      $block->title .= ' (' . ++$count . ')';
    }
  
    if (drupal_write_record('adtech_blocks', $block)) {
      drupal_set_message($block->title . ' block added.');
    }
    else {
      drupal_set_message('There was an error adding ' . $block->title . ' block. Please try again.');
    }
  }
}

/**
 * Callback function to confirm user wishes to delete block
 */
function adtech_admin_delete_confirm_block($form_state, $block_id) {
  $block = adtech_block_load($block_id);
  $form['block_id'] = array(
    '#type' => 'value',
    '#value' => $block_id,
  );

  $output = confirm_form($form, t('Are you sure you want to delete block %title?', array('%title' => $block['title'])), 'admin/settings/adtech/blocks');

  return $output;
}

/**
 * Confirmation function to delete block
 */
function adtech_admin_delete_confirm_block_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    adtech_admin_delete_block($form_state['values']['block_id']);
    $form_state['redirect'] = 'admin/settings/adtech/blocks';
    return;
  }
}

/**
 * Database call to do the actual deletion and optimise database to prevent overhead
 */
function adtech_admin_delete_block($block_id = 0) {
  db_query('DELETE FROM {adtech_blocks} WHERE block_id = %d LIMIT %d', $block_id, 1);
  db_query('OPTIMIZE TABLE {adtech_blocks}');
  drupal_set_message(t('The block has been deleted.'));
}

/**
 * ADTECH settings page
 */
function adtech_main_settings() {
  $form['help'] = array(
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#title'       => t('Help and instructions'),
  );
  $form['help']['help'] = array(
    '#type'  => 'markup',
    '#value' => '<p>The HELP text goes here</p>
    <h2>Tag Formats</h2>
    <p>IFRAME requires the DOCTYPE to be Transitional.</p>',
  );
  $form['siteconfig'] = array(
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#title'       => t('Site configuration'),
  );
  $form['siteconfig']['adtech_network_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Customer network ID'),
    '#size'          => 16,
    '#required'      => TRUE,
    '#default_value' => variable_get('adtech_network_id', NULL),
    '#description'   => t('This is the network ID for this site or family of sites. It should be a XXX digit number similar to %id.', array('%id' => '999')),
  );
  $form['siteconfig']['adtech_network_name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Network name'),
    '#required'      => TRUE,
    '#default_value' => variable_get('adtech_network_name', variable_get('site_name', NULL)),
    '#description'   => t('This is the name of the network. It defaults to the site name set in <a href="@siteinfo">site information</a>.', array('@siteinfo' => url('admin/settings/site-information'))),
  );
  $form['siteconfig']['adtech_site_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Site url'),
    '#required'      => TRUE,
    '#default_value' => variable_get('adtech_site_url', $_SERVER['SERVER_NAME']),
    '#description'   => t('The URL of the site the ADTECH advertisements will be placed on.'),
  );
  $form['advertconfig'] = array(
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#title'       => t('Advert configuration'),
  );
  $form['advertconfig']['adtech_tag_type'] = array(
    '#type'          => 'radios',
    '#title'         => t('ADTECH tag format'),
    '#required'      => TRUE,
    '#default_value' => variable_get('adtech_tag_type', 'javascript'),
    '#options'       => array(
      'iframe'     => t('IFRAME tag'),
      'javascript' => t('JavaScript tag'),
      'image'      => t('General image tag'),
      'newsletter' => t('Image tag for newsletters'),
    ),
    '#description'   => t('<p>Choose the type of HTML tag that will be generated.</p><p><strong class="error">WARNING!</strong> Changing this value will clear the block cache.</p>'),
  );
  $form['advertconfig']['adtech_tag_timestamp'] = array(
    '#type'          => 'radios',
    '#title'         => t('Type of timestamp to use'),
    '#default_value' => variable_get('adtech_tag_timestamp', 'server'),
    '#options'       => array(
      'server' => t('Server side'),
      'client' => t('Client side'),
    ),
    '#description'   => t('Choose to use either a client side or server side timestamp. This only applies when using the JavaScript tag above.'),
  );
  $form['advertconfig']['adtech_group_generator'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use Group ID generator'),
    '#default_value' => variable_get('adtech_group_generator', TRUE),
    '#description'   => t('If selected, automatically create a group ID each time a tag is loaded. <span class="warning">Group ID only works with Javascript tags and not IFRAMES.</span>'),
  );
  $form['advertconfig']['adtech_cookie'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use cookie targeting'),
    '#default_value' => variable_get('adtech_cookie', TRUE),
    '#description'   => t('If selected, the adverts will be able to use the cookie targeting feature of ADTECH.'),
  );
  $form['advertconfig']['adtech_auto_alias'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Generate placement aliases automatically'),
    '#default_value' => variable_get('adtech_auto_alias', FALSE),
    '#description'   => t('If selected, the placement IDs will be automatically generated using the pattern below.'),
  );
  $form['advertconfig']['adtech_auto_alias_pattern'] = array(
      '#type'        => 'textfield',
      '#title'       => t('Automatic advert alias pattern'),
      '#default_value' => variable_get('adtech_auto_alias_pattern', NULL),
      '#maxlength'   => 128,
      '#description' => t('Enter the pattern to use to automatically generate aliases. You can use tokens to set up patterns. Leave blank to not use aliases on the site.'),
    );
    $form['advertconfig']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['advertconfig']['token_help']['help'] = array(
      '#value' => theme('token_help', 'global'),
    );
  $form['advancedconfig'] = array(
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#title'       => t('Advanced configuration'),
  );
  $form['advancedconfig']['help'] = array(
    '#type'  => 'markup',
    '#value' => '<p>This section should only be altered if you have an understanding of the parameters in question and their function. If you require futher information on these, contact ADTECH Client Services.</p>',
  );
  $form['advancedconfig']['adtech_tag_domain'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Tag domain'),
    '#required'      => TRUE,
    '#default_value' => variable_get('adtech_tag_domain', 'adserver.adtech.de'),
    '#description'   => t('The default tag domain. If you can add your own tag domain to your Domain Name Service (DNS) you can change this option.'),
  );
  $form['advancedconfig']['adtech_tag_version'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Tag version'),
    '#size'          => 16,
    '#required'      => TRUE,
    '#default_value' => variable_get('adtech_tag_version', '3.0'),
    '#description'   => t('This is the tag version number. Only change this if you know what you are doing.'),
  );
  return system_settings_form($form);
}

/**
 * Validate the settings form
 *
 * Ensure Network ID and Tag version are valid numbers and not alphanumeric
 */
function adtech_main_settings_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['adtech_network_id'])) {
    form_set_error('adtech_network_id', t('Customer network ID must be a valid number.'));
  }
  if (!is_numeric($form_state['values']['adtech_tag_version'])) {
    form_set_error('adtech_tag_version', t('Tag version must be a valid number.'));
  }
  if ($form_state['values']['adtech_tag_type'] != variable_get('adtech_tag_type', 'javascript')) {
    cache_clear_all(NULL, 'cache_block');
    drupal_set_message('Block cache has been cleared.', 'warning');
  }
}

/**
 * Returns array of all adverts for the site
 */
function adtech_get_ads() {
  static $adverts = array();

  if (empty($adverts)) {
    $result = db_query("SELECT advert_id, alias, placement_id, size_id, block_id, visibility, pages, taxonomy, keyword_1, keyword_2, keyword_3, keyword_4, keyword_5, keyword_6, keyword_7, keyword_8, keyword_9, keyword_10, group_id, created, status FROM {adtech_adverts}");
    while ($row = db_fetch_array($result)) {
      $adverts[$row['advert_id']] = $row;
    }
  }
  return $adverts;
}

/**
 * Returns an array of a single advert
 */
function adtech_advert_load($advert_id = NULL) {
  if (is_numeric($advert_id)) {
    $result = db_fetch_array(db_query('SELECT advert_id, alias, placement_id, size_id, block_id, visibility, pages, taxonomy, keyword_1, keyword_2, keyword_3, keyword_4, keyword_5, keyword_6, keyword_7, keyword_8, keyword_9, keyword_10, group_id, created, status FROM {adtech_adverts} WHERE advert_id = %d', $advert_id));
    return $result;
  }
  else {
    return FALSE;
  }
}

/**
 * Returns an array of a single ADTECH block
 */
function adtech_block_load($block_id = NULL) {
  if (is_numeric($block_id)) {
    $result = db_fetch_array(db_query('SELECT block_id, title, caching, status FROM {adtech_blocks} WHERE block_id = %d', $block_id));
    return $result;
  }
  else {
    return FALSE;
  }
}

/**
 * Returns an array containing Drupal's block caching options
 */
function adtech_caching_options() {
  return array(
    BLOCK_CACHE_GLOBAL   => t('BLOCK_CACHE_GLOBAL'),
    BLOCK_CACHE_PER_PAGE => t('BLOCK_CACHE_PER_PAGE'),
    BLOCK_CACHE_PER_ROLE => t('BLOCK_CACHE_PER_ROLE'),
    BLOCK_CACHE_PER_USER => t('BLOCK_CACHE_PER_USER'),
    BLOCK_NO_CACHE       => t('BLOCK_NO_CACHE'),
  );
}

/**
 * Returns an array of the advert visibility settings
 */
function adtech_get_visibility_options() {
  return array(
    t('Show on every page except the listed pages.'),
    t('Show on only the listed pages.'),
    t('Default creative - shown on every page where no other creative is set to display'),
  );
}

