ADTECH Module


INTRODUCTION
------------

Current Maintainer: Gideon Cresswell (SkidNCrashwell)
<https://www.drupal.org/u/skidncrashwell>

Allow site owners to display advertisement campaigns generated using the ADTECH
<http://www.adtech.com> system to earn revenue.

Depends on the Token module <https://www.drupal.org/project/token>.

